//
//  DetailInteractor.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation


protocol ProtocolDetailInteractor {
    var presenter: ProtocolDetailPresenter? { get set }
    
    func interactorCallService(type: ListType)
}


class DetailInteractor: ProtocolDetailInteractor {
    var presenter: ProtocolDetailPresenter?
    
    func interactorCallService(type: ListType) {
        let url = String(format: "%@%@%@%@", BASE_GENRES_URL, type.rawValue, COMPLEMENT_GENRES_URL, API_KEY)
        invokeServerforOperation(url: url, operation: OPERATION_GET_GENRES)
    }
    
    
    func invokeServerforOperation(url: String, operation: Int) {
        Server.sharedInstance.simpleGetUrl(fromURLString: url, operationCode: operation) { (operationcode, result) in
            Loader.sharedInstance.hideIndicator()
            switch operationcode {
            case OPERATION_GET_GENRES:
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                case .success(let data):
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print("Data received:\(json)")
                        guard let response: NSDictionary = json as? NSDictionary else { return }
                        self.presenter?.presenterReceiveGenres(genres: response.value(forKey: "genres") as! NSArray)
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                break
            default:
                print("no hay manejador para la respuesta...")
            }
        }
    }
}

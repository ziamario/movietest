//
//  DetailViewController.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import UIKit

protocol ProtocolDetailView {
    var presenter: ProtocolDetailPresenter? { get set }
    var movieInfo: NSDictionary? { get set }
    var type: ListType? { get set }
    
    func viewReceiveGenres(genres: NSArray)
}

class DetailViewController: UIViewController, ProtocolDetailView {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewBar: UIView!
    @IBOutlet weak var generalView: UIView!
    
    var presenter: ProtocolDetailPresenter?
    var movieInfo: NSDictionary?
    var type: ListType?
    var allGenres: NSArray!

    override func viewDidLoad() {
        super.viewDidLoad()
        Loader.sharedInstance.showIndicator(view: self)
        presenter?.presenterCallService(type: self.type!)
        configureComponents()
        setColor()
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        presenter?.presenterBackAction()
    }
    
    func setColor() {
        self.viewBar.backgroundColor = UIColor(named: HEADER_COLOR)
        self.generalView.backgroundColor = .black
    }
    
    func configureComponents() {
        self.lblTitle.text = (movieInfo!.value(forKey: (self.type == .movieType) ? TITLE_KEYWORD: NAME_KEYWORD) as! String)
        self.lblDescription.text = (movieInfo!.value(forKey: OVERVIEW_KEYWORD) as! String)
        self.lblOverview.text = Strings.DetailScreen.overViewText
        self.imgMovie.layer.cornerRadius = 10
        //
        let imgUrl = String(format: "%@%@", BASE_IMAGE_URL, (movieInfo!.value(forKey: BACKDROP_PATH_KEYWORD) as! String))
        getImage(poster: imgUrl)
    }
    
    func getImage(poster: String) {
        DispatchQueue.global(qos: .background).async {
            let imgUrl = NSURL( string: poster)
            let imageData = NSData(contentsOf: imgUrl! as URL)
            DispatchQueue.main.async {
                self.imgMovie.image = UIImage.init(data: imageData! as Data)
            }
        }
    }
    
    func viewReceiveGenres(genres: NSArray) {
        self.allGenres = genres
        searchGenres()
    }
    
    func searchGenres() {
        var genresText = ""
        
        for currentGenre in allGenres {
            let auxGenre = (currentGenre as! NSDictionary)
            let idsArray = (movieInfo!.value(forKey: "genre_ids") as! NSArray)
            let idValue: Int = (auxGenre.value(forKey: "id") as! Int)
            for dataIds in idsArray {
                if idValue == (dataIds as! Int) {
                    let newGenre = String(format: "%@,", (auxGenre.value(forKey: "name") as! String))
                    genresText = String(format: "%@ %@", genresText, newGenre)
                }
            }
        }
        DispatchQueue.main.async {
            let removedLast: String = String(genresText.dropLast())
            self.lblGenres.text = removedLast
        }
    }
}

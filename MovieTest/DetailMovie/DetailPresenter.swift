//
//  DetailPresenter.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation

protocol ProtocolDetailPresenter {
    var view: ProtocolDetailView? { get set }
    var interactor: ProtocolDetailInteractor? { get set }
    var router: ProtocolDetailRouter? { get set }
    
    func presenterCallService(type: ListType)
    func presenterReceiveGenres(genres: NSArray)
    func presenterBackAction()
}

class DetailPresenter: ProtocolDetailPresenter {
    var view: ProtocolDetailView?
    var interactor: ProtocolDetailInteractor?
    var router: ProtocolDetailRouter?
    
    func presenterCallService(type: ListType) {
        interactor?.interactorCallService(type: type)
    }
    
    func presenterBackAction() {
        router?.routerBackAction()
    }
    
    func presenterReceiveGenres(genres: NSArray) {
        view?.viewReceiveGenres(genres: genres)
    }
}

//
//  DetailRouter.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation
import UIKit

protocol ProtocolDetailRouter {
    var view: (UIViewController & ProtocolDetailView)? { get }
    
    static func start(info: NSDictionary, type: ListType) -> ProtocolDetailRouter
    
    func routerBackAction()
}

class DetailRouter: ProtocolDetailRouter {
    var view: (UIViewController & ProtocolDetailView)?
    
    static func start(info: NSDictionary, type: ListType) -> ProtocolDetailRouter {
        let router = DetailRouter()
        
        var view: ProtocolDetailView = DetailViewController()
        var presenter: ProtocolDetailPresenter = DetailPresenter()
        var interactor: ProtocolDetailInteractor = DetailInteractor()
        
        view.presenter = presenter
        view.movieInfo = info
        view.type = type
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view
        interactor.presenter = presenter
        
        router.view = view as? (UIViewController & ProtocolDetailView)
        
        return router
    }
    
    func routerBackAction() {
        self.view?.navigationController?.popViewController(animated: true)
    }
}

//
//  Server.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation

public class Server {
    public static let sharedInstance = Server()
    
    func simpleGetUrl(fromURLString urlString: String, operationCode: Int, completion: @escaping (_ operationcode: Int, Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                
                if let error = error {
                    completion(operationCode, .failure(error))
                }
                
                if let data = data {
                    completion(operationCode, .success(data))
                }
            }
            urlSession.resume()
        }
    }
    
    func simplePostUrl(fromURLString urlString: String, parameters: [String: Any], operationCode: Int, completion: @escaping (_ opCode: Int, Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            let session = URLSession(configuration: .default)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            do {
                // convert parameters to Data and assign dictionary to httpBody of request
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
              } catch let error {
                print(error.localizedDescription)
                return
              }
            let task = session.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    completion(operationCode, .failure(error))
                }
                
                if let data = data {
                    completion(operationCode, .success(data))
                }
            }
            task.resume()
        }
    }
    /*
    func simpleGetUrlRequestWithErrorHandling(urlService: String, operationCode: Int) {
        let url = URL(string: urlService)!
        let request = URLRequest(url: url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { [self] (data, response, error) in
            if let errorLocal = error {
                print("Error: \(errorLocal)")
            } else if let data = data {
                print("Handle HTTP request response")
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    switch operationCode {
                    case OPERATION_REQUEST_TOKEN:
                        print("Response for OPERATION_REQUEST_TOKEN")
                        print("\(json)")
                        break
                    default:
                        print("no hay manejador para la respuesta...")
                    }
                } catch {
                    print("JSON error: \(error.localizedDescription)")
                }
            } else {
                print("Error inesperado")
            }
        }
        task.resume()
    }*/
}
 

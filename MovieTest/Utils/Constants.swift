//
//  Constants.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation

///services
let API_KEY = "bb08c901ddd0212fe29f2310ea277f82"
let BASE_API_URL = "https://api.themoviedb.org/3/"
let AUTHENTICATION_LOGIN = "authentication/token/validate_with_login?api_key="
let POPULAR_API_URL = "/popular?api_key="
let NOW_PLAYING_URL = "/now_playing?api_key="
let ON_THE_AIR_URL = "/on_the_air?api_key="
let BASE_IMAGE_URL = "https://image.tmdb.org/t/p/original"
let BASE_GENRES_URL = "https://api.themoviedb.org/3/genre/"
let COMPLEMENT_GENRES_URL = "/list?api_key="

///Server operations
let OPERATION_REQUEST_TOKEN             = 10001
let OPERATION_VALIDATE_LOGIN            = 10002
let OPERATION_FOR_POPULAR               = 10003
let OPERATION_FOR_NOW_PLAYING           = 10004
let OPERATION_ON_THE_AIR                = 10005
let OPERATION_GET_GENRES                = 10006

///colors
let DISABLE_BTN_COLOR           = "disableBtnColor"
let ENABLE_BTN_COLOR            = "enableBtnColor"
let WHITE_COLOR_TEXT            = "witheTextColor"
let COLOR_TEXT                  = "colorTextBtn"
let HEADER_COLOR                = "headerColor"

///keys
let TITLE_KEYWORD               = "title"
let RELEASE_DATE_KEYWORD        = "release_date"
let VOTE_AVERAGE_KEYWORD        = "vote_average"
let OVERVIEW_KEYWORD            = "overview"
let POSTER_PATH_KEYWORD         = "poster_path"
let NAME_KEYWORD                = "name"
let AIR_DATE_KEYWORD            = "first_air_date"
let VOTE_COUNT_KEYWORD          = "vote_count"
let BACKDROP_PATH_KEYWORD       = "backdrop_path"



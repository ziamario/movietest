//
//  Strings.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation


struct Strings {
    struct LoginScreen {
        static var userPlaceHolder: String {
            return "Username"
        }
        static var passwordPlaceHolder: String {
            return "Password"
        }
        static var loginButtonText: String {
            return "Log in"
        }
    }
    
    struct WelcomeScreen {
        static var moviesOption: String {
            return "Movies"
        }
        static var tvOption: String {
            return "TV Show"
        }
        static var playingOption: String {
            return "Playing"
        }
        static var popularOption: String {
            return "Popular"
        }
        
        static var alerMenuTitle: String {
            return "What do you want to do?"
        }
        
        static var emptyText: String {
            return ""
        }
        
        static var viewProfileOpt: String {
            return "View Profile"
        }
        
        static var logoutOption: String {
            return "Log out"
        }
        
        static var cancelOption: String {
            return "Cancel"
        }
    }
    
    struct DetailScreen {
        static var overViewText: String {
            return "overview"
        }
    }
}

//
//  WelcomeInteractor.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation

protocol ProtocolWelcomeInteractor {
    var presenter: ProtocolWelcomePresenter? { get set }
    
    func interactoraCallPopularService(type: ListType)
    func interactorCallPlayingService(type: ListType)
    func interactorCallAirService(type: ListType)
}

class WelcomeInteractor: ProtocolWelcomeInteractor {
    var presenter: ProtocolWelcomePresenter?
    
    func interactoraCallPopularService(type: ListType) {
        let url = String(format: "%@%@%@%@", BASE_API_URL, type.rawValue, POPULAR_API_URL, API_KEY)
        invokeServerforOperation(url: url, operation: OPERATION_FOR_POPULAR)
    }
    
    func interactorCallPlayingService(type: ListType) {
        let url = String(format: "%@%@%@%@", BASE_API_URL, type.rawValue, NOW_PLAYING_URL, API_KEY)
        invokeServerforOperation(url: url, operation: OPERATION_FOR_POPULAR)
    }
    
    func interactorCallAirService(type: ListType) {
        let url = String(format: "%@%@%@%@", BASE_API_URL, type.rawValue, ON_THE_AIR_URL, API_KEY)
        invokeServerforOperation(url: url, operation: OPERATION_FOR_POPULAR)
    }
    
    ///
    func invokeServerforOperation(url: String, operation: Int) {
        Server.sharedInstance.simpleGetUrl(fromURLString: url, operationCode: operation) { (operationcode, result) in
            Loader.sharedInstance.hideIndicator()
            switch operationcode {
            case OPERATION_FOR_POPULAR:
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                case .success(let data):
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print("Data received:\(json)")
                        guard let response: NSDictionary = json as? NSDictionary else { return }
                        
                        if let results = response.value(forKey: "results") {
                            self.presenter?.presenterReceiveResult(movies: results as! NSArray)
                            return
                        }
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                break
            case OPERATION_FOR_NOW_PLAYING:
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                case .success(let data):
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print("Data received:\(json)")
                        guard let response: NSDictionary = json as? NSDictionary else { return }
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                break
            case OPERATION_ON_THE_AIR:
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                case .success(let data):
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print("Data received:\(json)")
                        guard let response: NSDictionary = json as? NSDictionary else { return }
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                break
            default:
                print("no hay manejador para la respuesta...")
            }
            Loader.sharedInstance.hideIndicator()
        }
    }
}

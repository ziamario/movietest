//
//  CollectionMoviewCell.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import UIKit

class CollectionMoviewCell: UICollectionViewCell {
    @IBOutlet weak var principalView: UIView!
    @IBOutlet weak var auxView: UIView!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var coverImg: UIImageView!
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var icnRate: UIImageView!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setCornersView()
        setColor()
    }
    
    func setCornersView() {
        self.principalView.layer.cornerRadius = 10
        self.coverImg.layer.cornerRadius = 10
        self.viewData.layer.cornerRadius = 10
        self.viewData.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
//        self.imgMovie.layer.cornerRadius = 15
//        self.imgMovie.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    func setColor() {
        self.viewData.backgroundColor = UIColor(named: HEADER_COLOR)
    }
    
    func populateInfoCell(info: NSDictionary) {
        self.lblTitle.text = (info.value(forKey: TITLE_KEYWORD) as! String)
        self.lblDate.text = (info.value(forKey: RELEASE_DATE_KEYWORD) as! String)
        let rate: Double = info.value(forKey: VOTE_AVERAGE_KEYWORD) as! Double
        self.lblRate.text = String(format: "%.1f", rate)
        self.lblDescription.text = (info.value(forKey: OVERVIEW_KEYWORD) as! String)
        let urlImage = String(format: "%@%@", BASE_IMAGE_URL,(info.value(forKey: POSTER_PATH_KEYWORD) as! String))
        getImage(poster: urlImage)
    }
    
    func populateTVInfo(info: NSDictionary) {
        self.lblTitle.text = (info.value(forKey: NAME_KEYWORD) as! String)
        self.lblDate.text = (info.value(forKey: AIR_DATE_KEYWORD) as! String)
        let rate: Double = info.value(forKey: VOTE_COUNT_KEYWORD) as! Double
        self.lblRate.text = String(format: "%.1f", rate)
        self.lblDescription.text = (info.value(forKey: OVERVIEW_KEYWORD) as! String)
        let urlImage = String(format: "%@%@", BASE_IMAGE_URL,(info.value(forKey: POSTER_PATH_KEYWORD) as! String))
        getImage(poster: urlImage)
    }
    
    func getImage(poster: String) {
        DispatchQueue.global(qos: .background).async {
            let imgUrl = NSURL( string: poster)
            let imageData = NSData(contentsOf: imgUrl! as URL)
            DispatchQueue.main.async {
                self.coverImg.image = UIImage.init(data: imageData! as Data)
            }
        }
    }


}

//
//  WelcomeViewController.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import UIKit

protocol ProtocolWelcomeView {
    var presenter: ProtocolWelcomePresenter? { get set }
    
    func viewReceiveResults(movies: NSArray)
}

enum ListType: String {
    case movieType = "movie"
    case tvType = "tv"
}

class WelcomeViewController: UIViewController, ProtocolWelcomeView {
    @IBOutlet weak var lblGeneralTitle: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var sgmtControl: UISegmentedControl!
    @IBOutlet weak var btnPlayingM: UIButton!
    @IBOutlet weak var btnPopularM: UIButton!
    @IBOutlet weak var btnPlayingTV: UIButton!
    @IBOutlet weak var btnPopularTV: UIButton!
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var moviesCollection: UICollectionView!
    
    var presenter: ProtocolWelcomePresenter?
    var current: Int = 0
    var contentType: ListType = .movieType
    var moviesResult: NSArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalConfigurations()
        Loader.sharedInstance.showIndicator(view: self)
        presenter?.presenterCallService(type: contentType)
        viewSetUpCollectionObjs()
    }
    @IBAction func actionSegmanted(_ sender: Any) {
        if sgmtControl.selectedSegmentIndex == 0 {
            self.lblGeneralTitle.text = Strings.WelcomeScreen.moviesOption
        } else if sgmtControl.selectedSegmentIndex == 1 {
            self.lblGeneralTitle.text = Strings.WelcomeScreen.tvOption
        }
        configureBtnStatus(segmentSelected: sgmtControl.selectedSegmentIndex)
    }
    //
    func generalConfigurations() {
        self.navigationController?.isNavigationBarHidden = true
        self.lblGeneralTitle.text = Strings.WelcomeScreen.moviesOption
        self.moviesCollection.backgroundColor = .black
        self.sgmtControl.setTitle(Strings.WelcomeScreen.moviesOption, forSegmentAt: 0)
        self.sgmtControl.setTitle(Strings.WelcomeScreen.tvOption, forSegmentAt: 1)
        self.sgmtControl.selectedSegmentIndex = 0
        self.sgmtControl.addTarget(self, action: #selector(changeSelection(sender:)), for: .valueChanged)
        let segmenteTap = UITapGestureRecognizer.init(target: self, action: #selector(unitsSegTap))
        segmenteTap.cancelsTouchesInView = false
        self.sgmtControl.addGestureRecognizer(segmenteTap)
        let normalTintAtrrs = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let selectedTintAtrrs = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.sgmtControl.setTitleTextAttributes(normalTintAtrrs, for: .normal)
        self.sgmtControl.setTitleTextAttributes(selectedTintAtrrs, for: .selected)
        //
        self.headerView.backgroundColor = UIColor(named: HEADER_COLOR)
        self.generalView.backgroundColor = .black
        ///BUTTONS
        btnPlayingM.setTitle(Strings.WelcomeScreen.playingOption, for: .normal)
        btnPlayingM.backgroundColor = UIColor(named: DISABLE_BTN_COLOR)
        btnPlayingM.setTitleColor(UIColor(named: WHITE_COLOR_TEXT), for: .normal)
        btnPlayingM.addTarget(self, action: #selector(btnCategory(sender:)), for: .touchUpInside)
        btnPopularM.setTitle(Strings.WelcomeScreen.popularOption, for: .normal)
        btnPopularM.backgroundColor = UIColor(named: DISABLE_BTN_COLOR)
        btnPopularM.setTitleColor(UIColor(named: WHITE_COLOR_TEXT), for: .normal)
        btnPopularM.addTarget(self, action: #selector(btnCategory(sender:)), for: .touchUpInside)
        btnPopularTV.setTitle(Strings.WelcomeScreen.popularOption, for: .normal)
        btnPopularTV.backgroundColor = UIColor(named: DISABLE_BTN_COLOR)
        btnPopularTV.setTitleColor(UIColor(named: WHITE_COLOR_TEXT), for: .normal)
        btnPopularTV.addTarget(self, action: #selector(btnCategory(sender:)), for: .touchUpInside)
        btnPlayingTV.setTitle(Strings.WelcomeScreen.playingOption, for: .normal)
        btnPlayingTV.backgroundColor = UIColor(named: DISABLE_BTN_COLOR)
        btnPlayingTV.setTitleColor(UIColor(named: WHITE_COLOR_TEXT), for: .normal)
        btnPlayingTV.addTarget(self, action: #selector(btnCategory(sender:)), for: .touchUpInside)
        ///menu
        let menuTap = UITapGestureRecognizer.init(target: self, action: #selector(showSimpleActionSheet))
        self.imgMenu.addGestureRecognizer(menuTap)
        self.imgMenu.isUserInteractionEnabled = true
    }
    
    @objc func showSimpleActionSheet(sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: Strings.WelcomeScreen.alerMenuTitle, message: Strings.WelcomeScreen.emptyText, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Strings.WelcomeScreen.viewProfileOpt, style: .default, handler: { (_) in
            print("User click Approve button")
        }))
        
        alert.addAction(UIAlertAction(title: Strings.WelcomeScreen.logoutOption, style: .destructive, handler: { (_) in
            self.navigationController?.popViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: Strings.WelcomeScreen.cancelOption, style: .cancel, handler: { (_) in
        }))
        
        self.present(alert, animated: true, completion: {
        })
    }
    
    func configureBtnStatus(segmentSelected: Int) {
        current = segmentSelected
    }
    
    @objc func changeSelection(sender: UISegmentedControl) {
        updateStateBtn()
    }
    
    @objc func unitsSegTap(sender: UITapGestureRecognizer) {
        updateStateBtn()
    }
    
    @objc func btnCategory(sender: UIButton) {
        Loader.sharedInstance.showIndicator(view: self)
        self.btnPopularM.isHidden = true
        self.btnPlayingM.isHidden = true
        self.btnPlayingTV.isHidden = true
        self.btnPopularTV.isHidden = true
        let indexBtn = sender.tag
        switch indexBtn {
        case 0:
            print("playingM")
            presenter?.presenterCallPlayingService(type: contentType)
            break
        case 1:
            print("popularM")
            presenter?.presenterCallService(type: contentType)
            break
        case 2:
            print("playingTV")
            presenter?.presenterCallAirService(type: contentType)
            break
        default:
            print("popularTV")
            presenter?.presenterCallService(type: contentType)
            break
        }
    }
    
    func updateStateBtn() {
        if current == 0 {
            self.contentType = .movieType
            self.btnPopularM.isHidden = !btnPopularM.isHidden
            self.btnPlayingM.isHidden = !btnPlayingM.isHidden
            self.btnPlayingTV.isHidden = true
            self.btnPopularTV.isHidden = true
        } else if current == 1 {
            self.contentType = .tvType
            self.btnPopularM.isHidden = true
            self.btnPlayingM.isHidden = true
            self.btnPlayingTV.isHidden = !btnPlayingTV.isHidden
            self.btnPopularTV.isHidden = !btnPopularTV.isHidden
        }
    }
    
    func viewSetUpCollectionObjs() {
         let movieNib = UINib(nibName: "CollectionMoviewCell", bundle: nil)
        moviesCollection.register(movieNib, forCellWithReuseIdentifier: "collectionMoviewCell")
    }
    
    func viewReceiveResults(movies: NSArray) {
        self.moviesResult = movies
        DispatchQueue.main.async {
            self.moviesCollection.reloadData()
        }
    }
}

extension WelcomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.moviesResult != nil {
            return self.moviesResult.count
        }
        return 0
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionMoviewCell", for: indexPath as IndexPath) as! CollectionMoviewCell
        if contentType == .movieType {
            cell.populateInfoCell(info: self.moviesResult[indexPath.row] as! NSDictionary)
        } else {
            cell.populateTVInfo(info: self.moviesResult[indexPath.row] as! NSDictionary)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width / 2 - 1
        return CGSize(width: width, height: width * 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.presenterRedirectToDetail(currentMovie: self.moviesResult[indexPath.row] as! NSDictionary, type: self.contentType)
    }
    
}

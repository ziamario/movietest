//
//  WelcomeRouter.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation
import UIKit

protocol ProtocolWelcomeRouter {
    var view: (UIViewController & ProtocolWelcomeView)? { get }
    
    static func start() -> ProtocolWelcomeRouter
    
    func routerRedirectToDetail(selectedMovie: NSDictionary, type: ListType)
}

class WelcomeRouter: ProtocolWelcomeRouter {
    var view: (UIViewController & ProtocolWelcomeView)?
    
    static func start() -> ProtocolWelcomeRouter {
        let router = WelcomeRouter()
        
        var view: ProtocolWelcomeView = WelcomeViewController()
        var presenter: ProtocolWelcomePresenter = WelcomePresenter()
        var interactor: ProtocolWelcomeInteractor = WelcomeInteractor()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view
        interactor.presenter = presenter
        
        router.view = view as? (UIViewController & ProtocolWelcomeView)
        
        return router
    }
    
    func routerRedirectToDetail(selectedMovie: NSDictionary, type: ListType) {
        let detailRouter = DetailRouter.start(info: selectedMovie, type: type)
        DispatchQueue.main.async { () -> Void in
            self.view?.navigationController?.pushViewController(detailRouter.view!, animated: true)
        }
    }
    
}

//
//  WelcomePresenter.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation

protocol ProtocolWelcomePresenter {
    var view: ProtocolWelcomeView? { get set }
    var interactor: ProtocolWelcomeInteractor? { get set }
    var router: ProtocolWelcomeRouter? { get set }
    
    func presenterCallService(type: ListType)
    func presenterCallPlayingService(type: ListType)
    func presenterCallAirService(type: ListType)
    func presenterReceiveResult(movies: NSArray)
    func presenterRedirectToDetail(currentMovie: NSDictionary, type: ListType)
}


class WelcomePresenter: ProtocolWelcomePresenter {
    var view: ProtocolWelcomeView?
    var interactor: ProtocolWelcomeInteractor?
    var router: ProtocolWelcomeRouter?
    
    func presenterCallService(type: ListType) {
        interactor?.interactoraCallPopularService(type: type)
    }
    
    func presenterCallPlayingService(type: ListType) {
        interactor?.interactorCallPlayingService(type: type)
    }
    
    func presenterCallAirService(type: ListType) {
        interactor?.interactorCallAirService(type: type)
    }
    
    func presenterReceiveResult(movies: NSArray) {
        view?.viewReceiveResults(movies: movies)
    }
    
    func presenterRedirectToDetail(currentMovie: NSDictionary, type: ListType) {
        router?.routerRedirectToDetail(selectedMovie: currentMovie, type: type )
    }
}

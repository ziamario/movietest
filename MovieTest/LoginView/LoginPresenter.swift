//
//  LoginPresenter.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation

protocol ProtocolLoginPresenter {
    var router: ProtocolLoginRouter? { get set }
    var interactor: ProtocolLoginInteractor? { get set}
    var view: ProtocolLoginView? { get set }
    //
    func presenterDidLoad()
    func presenterGetAuthenticationToken()
    func presenterReceiveToken(token: String)
    func presenterValidateLogin(user: String, pass: String, token: String)
    func presenterReceiveLoginResponse(isSucess: Bool, message: String)
    //
    func presenterRedirectToWelcome()
}

class LoginPresenter: ProtocolLoginPresenter {
    var router: ProtocolLoginRouter?
    var interactor: ProtocolLoginInteractor?
    var view: ProtocolLoginView?
    
    func presenterDidLoad() {
        view?.viewExtraConfigurations()
    }
    
    func presenterGetAuthenticationToken() {
        interactor?.interactorGetAuthenticationToken()
    }
    
    func presenterReceiveToken(token: String) {
        view?.viewReceiveToken(token: token)
    }
    
    func presenterValidateLogin(user: String, pass: String, token: String) {
        interactor?.interactorValidateLogin(user: user, pass: pass, token: token)
    }
    
    func presenterReceiveLoginResponse(isSucess: Bool, message: String) {
        view?.viewReceiveLoginResponse(isSuccess: isSucess, mesage: message)
    }
    
    func presenterRedirectToWelcome() {
        router?.routerRedirectToWelcome()
    }
}

//
//  LoginInteractor.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation
import UIKit

protocol ProtocolLoginInteractor {
    var presenter: ProtocolLoginPresenter? { get set}
    //functions
    func interactorGetAuthenticationToken()
    func interactorValidateLogin(user: String, pass: String, token: String)
}

class LoginInteractor: ProtocolLoginInteractor {
    var presenter: ProtocolLoginPresenter?
    //
    func interactorGetAuthenticationToken() {
        let url = String(format: "%@authentication/token/new?api_key=%@", BASE_API_URL, API_KEY)
        
        invokeServerforOperation(url: url, operation: OPERATION_REQUEST_TOKEN)
    }
    
    func interactorValidateLogin(user: String, pass: String, token: String) {
        let url = String(format: "%@%@%@", BASE_API_URL, AUTHENTICATION_LOGIN, API_KEY)
        let params = NSMutableDictionary()
        params.setValue(user, forKey: "username")
        params.setValue(pass, forKey: "password")
        params.setValue(token, forKey: "request_token")
        
        invokeServerforPostOperation(url: url, operation: OPERATION_VALIDATE_LOGIN, params: params as! [String : Any])
    }
    
    
    func invokeServerforOperation(url: String, operation: Int) {
        Server.sharedInstance.simpleGetUrl(fromURLString: url, operationCode: operation) { (operationcode, result) in
            switch operationcode {
            case OPERATION_REQUEST_TOKEN:
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                case .success(let data):
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print("Data received:\(json)")
                        guard let response: NSDictionary = json as? NSDictionary else { return }
                        self.presenter?.presenterReceiveToken(token: response.value(forKey: "request_token") as! String)
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                break
            default:
                print("no hay manejador para la respuesta...")
            }
        }
    }
    
    func invokeServerforPostOperation(url: String, operation: Int, params: [String: Any]) {
        Server.sharedInstance.simplePostUrl(fromURLString: url, parameters: params, operationCode: operation) { (opCode, result) in
            switch opCode {
            case OPERATION_VALIDATE_LOGIN:
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                case .success(let data):
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print("Data received:\(json)")
                        guard let response: NSDictionary = json as? NSDictionary else { return }
                        if let status = response.value(forKey: "success") {
                            let success = status as! Bool
                            var message = ""
                            if !success {
                                message = response.value(forKey: "status_message") as! String
                            }
                            self.presenter?.presenterReceiveLoginResponse(isSucess: success, message: message)
                        }
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                break
            default:
                print("no hay manejador para la respuesta...")
            }
        }
    }
}

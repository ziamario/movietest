//
//  LoginRouter.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import Foundation
import UIKit

protocol ProtocolLoginRouter {
    var view: (UIViewController & ProtocolLoginView)? { get }
    static func start() -> ProtocolLoginRouter
    
    func routerRedirectToWelcome()
}


class LoginRouter: ProtocolLoginRouter {
    var view: (UIViewController & ProtocolLoginView)?
    
    static func start() -> ProtocolLoginRouter {
        let router = LoginRouter()
        
        var view: ProtocolLoginView = LoginViewController()
        var interactor: ProtocolLoginInteractor = LoginInteractor()
        var presenter: ProtocolLoginPresenter = LoginPresenter()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        interactor.presenter = presenter
        
        router.view = view as? (UIViewController & ProtocolLoginView)
        
        return router
    }
    
    func routerRedirectToWelcome() {
        let welcomeRouter = WelcomeRouter.start()
        DispatchQueue.main.async { () -> Void in
            self.view?.navigationController?.pushViewController(welcomeRouter.view!, animated: true)
        }
    }
    
}

//
//  LoginViewController.swift
//  MovieTest
//
//  Created by Mario Vargas on 04/2022.
//

import UIKit

protocol ProtocolLoginView {
    var presenter: ProtocolLoginPresenter? { get set }
    //
    func viewReceiveToken(token: String)
    func viewExtraConfigurations()
    func viewReceiveLoginResponse(isSuccess: Bool, mesage: String)
}

class LoginViewController: UIViewController, ProtocolLoginView {
    @IBOutlet weak var icnLogin: UIImageView!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblError: UILabel!
    //
    var isUserOk = false
    var isPassOk = false
    
    
    var presenter: ProtocolLoginPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.presenterDidLoad()
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        Loader.sharedInstance.showIndicator(view: self)
        presenter?.presenterGetAuthenticationToken()
    }
    
    @objc func updateUserField(_ textField: UITextField) {
        let user = textField.text ?? ""
        isUserOk = user.count > 0
        validateButtonStatus()
    }
    
    @objc func updatePassField(_ textField: UITextField) {
        let pass = textField.text ?? ""
        isPassOk = pass.count > 0
        validateButtonStatus()
    }
    
    func viewReceiveToken(token: String) {
        Loader.sharedInstance.hideIndicator()
        presenter?.presenterValidateLogin(user: txtUserName.text!, pass: txtPassword.text!, token: token)
    }
    
    func viewExtraConfigurations() {
        self.txtUserName.placeholder = Strings.LoginScreen.userPlaceHolder
        self.txtUserName.addTarget(self, action: #selector(self.updateUserField(_:)), for: .editingChanged)
        self.txtPassword.placeholder = Strings.LoginScreen.passwordPlaceHolder
        self.txtPassword.addTarget(self, action: #selector(self.updatePassField(_:)), for: .editingChanged)
        //
        self.btnLogin.setTitle(Strings.LoginScreen.loginButtonText, for: .normal)
        self.btnLogin.isEnabled = false
        self.btnLogin.backgroundColor = UIColor(named: DISABLE_BTN_COLOR)
        self.btnLogin.setTitleColor(UIColor(named: WHITE_COLOR_TEXT), for: .normal)
    }
    
    func validateButtonStatus() {
        self.btnLogin.isEnabled = isUserOk && isPassOk
        self.btnLogin.backgroundColor = UIColor(named: (isUserOk && isPassOk) ? ENABLE_BTN_COLOR: DISABLE_BTN_COLOR)
        self.btnLogin.setTitleColor(UIColor(named: (isUserOk && isPassOk) ? COLOR_TEXT: WHITE_COLOR_TEXT), for: .normal)
    }
    
    func viewReceiveLoginResponse(isSuccess: Bool, mesage: String) {
        Loader.sharedInstance.hideIndicator()
        if isSuccess {
            presenter?.presenterRedirectToWelcome()
        } else {
            DispatchQueue.main.async {
                self.lblError.text = mesage
                self.lblError.isHidden = isSuccess
            }
        }
    }
}
